//
//  CLSDatabase.swift
//  CouchbaseLiteSwift
//
//  Created by Marco Betschart on 19.09.16.
//  Copyright © 2016 MANDELKIND. All rights reserved.
//

import Foundation
import CouchbaseLite

public typealias CLSDatabaseSchemaMigration = (CLSDatabase,UInt64) throws -> Void
public typealias CLSDatabaseSetup = (CLSDatabase) throws -> Void


/**
Configuration class for your Database.
To change the configuration of the default database use the following:


```
let config = CLSDatabase.defaultConfiguration()
config.schemaVersion = 3

CLSDatabase.defaultConfiguration = config
```
*/
open class CLSConfiguration: Equatable{
	/// Returns a Boolean value indicating whether two values are equal.
	///
	/// Equality is the inverse of inequality. For any values `a` and `b`,
	/// `a == b` implies that `a != b` is `false`.
	///
	/// - Parameters:
	///   - lhs: A value to compare.
	///   - rhs: Another value to compare.
	public static func ==(lhs: CLSConfiguration, rhs: CLSConfiguration) -> Bool {
		return ObjectIdentifier(lhs) == ObjectIdentifier(rhs)
	}

	open var directory = CBLManager.defaultDirectory()
	open var databaseName = "database"
	open var databaseSetup: CLSDatabaseSetup? = nil
	open var schemaMigration: CLSDatabaseSchemaMigration? = nil
	open var schemaVersion: UInt64 = 0
	
	//added to circumvent "error opening!: 23" - for details see https://github.com/couchbase/couchbase-lite-ios/issues/1405
	open var readOnly = false
	open var fileProtection: NSData.WritingOptions = .completeFileProtectionUnlessOpen
	
	
	open lazy var managerOptions: CBLManagerOptions = CBLManagerOptions.init(readOnly: self.readOnly, fileProtection: self.fileProtection)
	
	open var managerOptionsPointer: UnsafeMutablePointer<CBLManagerOptions>{
		let pointer = UnsafeMutablePointer<CBLManagerOptions>.allocate(capacity: 1)
		pointer.initialize(to: self.managerOptions)
		
		return pointer
	}
	
	
	private init(){}
	
	public init(databaseName: String){
		self.databaseName = databaseName
	}
	
	public init(databaseName: String, directory: String){
		self.databaseName = databaseName
		self.directory = directory
	}
	
	public static let standard = CLSConfiguration()
}


/**
The CLSDatabase class handles the connections and schema updates of your CouchbaseLite database.
*/
open class CLSDatabase {
	
	struct Static {
		static var maintainIfNeeded = true
	}
	
	
	open static var standardConfiguration: CLSConfiguration{
		return CLSConfiguration.standard
	}
	open static var standard = CLSDatabase(configuration: CLSDatabase.standardConfiguration)
	
	
	open static var standardManager: CBLManager? = {
		do{
			return try CBLManager(directory: CLSConfiguration.standard.directory, options:CLSConfiguration.standard.managerOptionsPointer)
		} catch let error{
			print(error)
		}
		
		return nil
	}()
	
	
	open func connection() throws -> CBLDatabase{
		return try connection(enableConnectionCache: true)
	}
	
	
	open func connection(enableConnectionCache: Bool) throws -> CBLDatabase{
		if enableConnectionCache, CLSDatabase.standardConfiguration == self.configuration{
			return try CLSDatabase.standardManager!.databaseNamed(CLSDatabase.standardConfiguration.databaseName)
		}
		return try CBLManager(directory: self.configuration.directory, options: self.configuration.managerOptionsPointer).databaseNamed(self.configuration.databaseName)
	}
	
	
	open var configuration: CLSConfiguration
	
	open var path: String?{
		return self.url.path
	}
	
	open var url: URL{
		return URL(fileURLWithPath: self.configuration.directory, isDirectory: true)
			.appendingPathComponent(self.configuration.databaseName)
			.appendingPathExtension("cblite2")
	}
	
	open var schemaVersion: UInt64?{
		get{
			if let schemaVersion = UserDefaults.standard.value(forKey: "DatabaseSchemaVersion$\(self.configuration.databaseName)") as? NSNumber{
				return schemaVersion.uint64Value
			}
			
			return nil
		}
		set{
			if let schemaVersion = newValue{
				UserDefaults.standard.setValue(NSNumber(value: schemaVersion as UInt64), forKey: "DatabaseSchemaVersion$\(self.configuration.databaseName)")
				UserDefaults.standard.synchronize()
			}
		}
	}
	
	open var needsSchemaMigration: Bool{
		if self.schemaVersion == nil, let setupDidRunAt = self.setupDidRunAt , setupDidRunAt.timeIntervalSinceNow < -10{
			return true
			
		} else if let schemaVersion = self.schemaVersion , schemaVersion < self.configuration.schemaVersion{
			return true
		}
		
		return false
	}
	
	open var needsSetup: Bool{
		return self.setupDidRunAt == nil
	}
	
	fileprivate var setupDidRunAt: Date?{
		get{
			return UserDefaults.standard.value(forKey: "DatabaseSetupDidRun$\(self.configuration.databaseName)") as? Date
		}
		set{
			UserDefaults.standard.setValue(newValue, forKey: "DatabaseSetupDidRun$\(self.configuration.databaseName)")
			UserDefaults.standard.synchronize()
		}
	}
	
	
	public convenience init( databaseName: String ){
		self.init( configuration: CLSConfiguration(databaseName: databaseName) )
	}
	
	
	public init(configuration: CLSConfiguration){
		self.configuration = configuration
		
		//maintenance is done only once in the lifetime of the app
		guard Static.maintainIfNeeded else {
			return
		}
		Static.maintainIfNeeded = false
		
		if self.needsSetup, let databaseSetup = self.configuration.databaseSetup{
			do{
				try databaseSetup(self)
				
			} catch let error as NSError{
				fatalError(error.description)
			}
			
			self.setupDidRunAt = Date()
			if self.schemaVersion == nil{
				self.schemaVersion = self.configuration.schemaVersion
			}
		}
		
		if self.needsSchemaMigration, let schemaMigration = self.configuration.schemaMigration{
			let oldSchemaVersion = self.schemaVersion ?? 0
			
			do{
				try schemaMigration(self,oldSchemaVersion)
				self.schemaVersion = self.configuration.schemaVersion
				
			} catch let error as NSError{
				fatalError(error.description)
			}
		}
	}
}

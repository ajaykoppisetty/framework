//
//  CLSQuery.swift
//  CouchbaseLiteSwift
//
//  Created by Marco Betschart on 19.09.16.
//  Copyright © 2016 MANDELKIND. All rights reserved.
//

import Foundation
import CouchbaseLite


public enum CLSQuerySortOrder: String{
	case ASC = ""
	case DESC = "-"
}


internal struct CLSQuerySort{
	var property: String?
	var order: CLSQuerySortOrder?
	
	init(property: String, order: CLSQuerySortOrder){
		self.property = property
		self.order = order
	}
}


private var CLSQueryBuilderCache = [String: CBLQueryBuilder]()

/**
Query your CouchbaseLite database with this super easy wrapper.
*/
open class CLSQuery<T: CLSModel>: LazySequenceProtocol,Collection{
	fileprivate var enumerator: CBLQueryEnumerator?
	fileprivate var format: String?
	fileprivate var params: [String: AnyObject]?
	fileprivate var sorts = [CLSQuerySort]()
	fileprivate var database: CLSDatabase?
	fileprivate var tiedToConnection: CBLDatabase?
	
	open var title: String?
	open var type: CLSModel.Type?
	
	
	convenience public init(){
		self.init(database: CLSDatabase.standard, type: T.self, title: nil)
	}
	
	
	convenience public init(connection: CBLDatabase){
		self.init(connection: connection, type: T.self, title: nil)
	}
	
	
	convenience public init(connection: CBLDatabase, type: CLSModel.Type?){
		self.init(connection: connection, type: type, title: nil)
	}
	
	
	public init(connection: CBLDatabase, type: CLSModel.Type?, title: String?){
		self.tiedToConnection = connection
		self.type = type
		self.title = title
	}
	
	
	convenience public init(title: String?){
		self.init(database: CLSDatabase.standard, type: T.self, title: title)
	}
	
	
	convenience public init(database: CLSDatabase){
		self.init(database: database, type: T.self, title: nil)
	}
	
	
	convenience public init(database: CLSDatabase, title: String?){
		self.init(database: database, type: T.self, title: title)
	}
	
	
	convenience public init(type: CLSModel.Type?){
		self.init(database: CLSDatabase.standard, type: type, title: nil)
	}
	
	
	convenience public init(type: CLSModel.Type?, title: String?){
		self.init(database: CLSDatabase.standard, type: type, title: title)
	}
	
	
	convenience public init(database: CLSDatabase, type: CLSModel.Type?){
		self.init(database: CLSDatabase.standard, type: type, title: nil)
	}
	
	
	public init(database: CLSDatabase, type: CLSModel.Type?, title: String?){
		self.database = database
		self.type = type
		self.title = title
	}
	
	
	fileprivate func connection() throws -> CBLDatabase{
		if let connection = self.tiedToConnection{
			return connection
		}
		
		return try database!.connection()
	}
	
	
	/**
	Limit the query results by setting condtions using NSPredicate syntax.
	
	Simple query with hard coded parameters:
	```
	.conditions("age > 15")
	```
	
	Conditions using dynamic parameters:
	
	```
	.conditions("givenName == $givenName")
	```
	
	- SeeAlso:
	- [NSPredicate - NSHipster](http://nshipster.com/nspredicate/)
	- [NSPredicate Class Reference](https://developer.apple.com/library/mac/documentation/Cocoa/Reference/Foundation/Classes/NSPredicate_Class/)
	*/
	open func conditions(_ format: String) -> CLSQuery<T>{
		self.format = format.characters.count > 0 ? format : nil
		
		return self
	}
	
	
	/**
	Limit the query results by setting condtions using NSPredicate syntax.
	This function uses query notation for specifying complex query in a easy readable format:
	
	```
	.conditions([
	"givenName == $givenName",
	"AND familyName == $familyName",
	"AND age > 15"
	])
	```
	
	- SeeAlso:
	- [NSPredicate - NSHipster](http://nshipster.com/nspredicate/)
	- [NSPredicate Class Reference](https://developer.apple.com/library/mac/documentation/Cocoa/Reference/Foundation/Classes/NSPredicate_Class/)
	*/
	open func conditions(_ format: [String]) -> CLSQuery<T>{
		self.format = format.count > 0 ? format.joined(separator: " ") : nil
		
		return self
	}
	
	
	/**
	Pass in NSPredicate parameters for dynamic queries:
	
	```
	.params([
	"givenName": "Marco",
	"familyName": "Betschart"
	])
	```
	
	- SeeAlso:
	- [NSPredicate - NSHipster](http://nshipster.com/nspredicate/)
	- [NSPredicate Class Reference](https://developer.apple.com/library/mac/documentation/Cocoa/Reference/Foundation/Classes/NSPredicate_Class/)
	*/
	open func params(_ params: [String: AnyObject]?) -> CLSQuery<T>{
		self.params = params
		
		return self
	}
	
	
	/**
	Sort the query:
	
	```
	.sort("familyName",order: .ASC)
	.sort("age",order: .DESC)
	```
	*/
	open func sort(_ property: String, order: CLSQuerySortOrder = .ASC) -> CLSQuery<T>{
		self.sorts.append( CLSQuerySort(property: property, order: order) )
		
		return self
	}
	
	
	/**
	Convenience function to mass delete all documents of the query
	*/
	open func deleteDocuments() throws -> CLSQuery<T>{
		for model in self{
			try model.deleteDocument()
		}
		
		return self
	}
	
	
	/**
	Get a simple array of T
	*/
	open func models() -> [T]{
		return self.map{
			return $0
		}
	}
	
	
	fileprivate func fetch(_ force: Bool = false){
		if force || self.enumerator == nil{
			self.enumerator = nil
			
			let wherePredicate = NSPredicate(format: {
				if let type = self.type?.valueOfTypeProperty(), let whereFormat = self.format{
					return "type == '\(type)' AND ( \(whereFormat) )"
					
				} else if let type = self.type?.valueOfTypeProperty(){
					return "type == '\(type)'"
					
				} else if let whereFormat = self.format{
					return whereFormat
				}
				
				return "1 == 1"
				}())
			
			let context: [String: AnyObject] = {
				func serialize(_ value: AnyObject?) -> AnyObject?{
					if let model = value as? CLSModel{
						return (model.document?.documentID ?? "") as AnyObject
						
					} else if let oldValue = value as? [AnyObject]{
						var newValue = [AnyObject]()
						
						for oldItem in oldValue{
							if let newItem = serialize(oldItem){
								newValue.append(newItem)
							}
						}
						
						return newValue as AnyObject
					}
					
					return value
				}
				
				if var context = self.params{
					for (key,value) in context{
						context[key] = serialize(value)
					}
					
					return context
				}
				
				return [:]
			}()
			
			let orderBy: [String] = self.sorts.map{ sort in
				if let property = sort.property, let order = sort.order{
					return "\(order.rawValue)\(property)"
					
				} else if let property = sort.property{
					return property
				}
				
				return ""
			}
			
			do{
				let newQueryBuilder = { (connection: CBLDatabase) in
					return try CBLQueryBuilder(
						database: connection,
						select: nil,
						wherePredicate: wherePredicate,
						orderBy: orderBy
					)
				}
				
				guard Thread.isMainThread else{ //query builder caching is only available in the main thread
					self.enumerator = try newQueryBuilder(self.tiedToConnection ?? CLSDatabase.standard.connection()).runQuery(withContext: context)
					return
				}
				let queryBuilderCacheId = wherePredicate.description + " ORDER BY " + orderBy.joined(separator: " ")
				
				if CLSQueryBuilderCache[queryBuilderCacheId] == nil{
					CLSQueryBuilderCache[queryBuilderCacheId] = try newQueryBuilder(self.tiedToConnection ?? self.connection())
				}
				
				if let queryBuilder = CLSQueryBuilderCache[queryBuilderCacheId]{
					self.enumerator = try queryBuilder.runQuery(withContext: context)
				}
				
			} catch let error {
				print(error)
			}
		}
	}
	
	
	open func makeIterator() -> AnyIterator<T> {
		self.fetch(true)
		
		return AnyIterator{
			guard let enumerator = self.enumerator, let nextRow = enumerator.nextRow(), let document = nextRow.document else {
				return nil
			}
			
			return self.type?.init(for: document) as? T
		}
	}
	
	
	open var count: Int {
		self.fetch(true)
		
		if let enumerator = self.enumerator{
			return Int(enumerator.count)
		}
		
		return 0
	}
	
	
	open var startIndex: Int {
		return 0
	}
	
	
	open var endIndex: Int {
		self.fetch(false)
		
		if let enumerator = self.enumerator{
			return Int(enumerator.count)
		}
		
		return 0
	}
	
	
	open subscript(i: Int) -> T {
		self.fetch(false)
		
		guard let enumerator = self.enumerator else{
			fatalError("enumerator is nil")
		}
		
		let count = Int(enumerator.count)
		guard count > i else {
			fatalError("index out of range")
		}
		
		guard let type = self.type else{
			fatalError("type is nil")
		}
		
		let document = enumerator.row(at: UInt(i)).document
		guard let doc = document else {
			fatalError("document is nil")
		}
		
		return type.init(for: doc) as! T
	}
	
	
	public func index(after i: Int) -> Int {
		return i + 1
	}
}
